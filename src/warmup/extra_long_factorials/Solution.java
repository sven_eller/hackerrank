package warmup.extra_long_factorials;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;

import static java.lang.System.out;
import static java.math.BigInteger.valueOf;


public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int input = Integer.parseInt(br.readLine());
        BigInteger factorial = BigInteger.ONE;
        for(int i = 1; i <= input; i++){
          factorial = factorial.multiply(valueOf(i));
        }
        out.println(factorial.toString());
    }
}
