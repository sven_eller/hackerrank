package warmup.simple_array_sum;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import static java.lang.System.out;
import static java.util.Arrays.*;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        br.readLine();
        out.println(stream(br.readLine().split(" ")).mapToInt(Integer::parseInt).sum());
    }
}
