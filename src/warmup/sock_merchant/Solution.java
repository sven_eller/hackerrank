package warmup.sock_merchant;

import java.io.*;
import java.util.*;

public class Solution {

    public static int sockMerchant(int n, int[] ar) {
        int[] colors = new int[100];

        for(int color: ar){
            colors[color - 1]++;
        }

        int sum = 0;
        for(int color: colors){
            if(color % 2 == 0){
                sum = sum + color / 2;
            } else {
                sum = sum + (color - 1) / 2;
            }

        }
        return sum;
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        int n = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        int[] ar = new int[n];

        String[] arItems = scanner.nextLine().split(" ");
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int i = 0; i < n; i++) {
            int arItem = Integer.parseInt(arItems[i]);
            ar[i] = arItem;
        }

        int result = sockMerchant(n, ar);

        bufferedWriter.write(String.valueOf(result));
        bufferedWriter.newLine();

        bufferedWriter.close();

        scanner.close();
    }
}
