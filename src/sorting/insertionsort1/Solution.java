package sorting.insertionsort1;


import java.util.*;

public class Solution {

    public static void insertIntoSorted(int[] ar) {
        int numberToSort = ar[ar.length - 1];

        for(int i = ar.length - 2; i >= 0; i--){
            if(ar[i] > numberToSort){
                ar[i + 1] = ar[i];
                printArray(ar);
            } else {
              ar[i + 1] = numberToSort;
              printArray(ar);
              return;
            }
        }
        ar[0] = numberToSort;
        printArray(ar);

    }


    /* Tail starts here */
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int s = in.nextInt();
        int[] ar = new int[s];
        for(int i=0;i<s;i++){
            ar[i]=in.nextInt();
        }
        insertIntoSorted(ar);
    }


    private static void printArray(int[] ar) {
        for(int n: ar){
            System.out.print(n+" ");
        }
        System.out.println("");
    }

}
