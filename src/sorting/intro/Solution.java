package sorting.intro;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashSet;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        br.readLine();
        String inputs = br.readLine();
        String[] split = inputs.split(" ");

        HashSet<String> set = new HashSet<String>(Arrays.asList(split));

        System.out.println(set.size() == split.length ? "YES" : "NO");
    }
}
