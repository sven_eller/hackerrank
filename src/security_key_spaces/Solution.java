package security_key_spaces;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String message = br.readLine();
        String key = br.readLine();
        StringBuilder builder = new StringBuilder();
        for(int i = 0; i < message.length(); i++){
            builder.append(shift(Character.getNumericValue(message.charAt(i)), Integer.parseInt(key)));
        }
        System.out.println(builder.toString());

    }

    public static int shift(int input, int key){
        return (input + key) % 10;
    }
}
