package jumping_on_the_clouds;

import org.junit.Test;

import static org.junit.Assert.*;

public class SolutionTest {

    @Test
    public void jumpingOnClouds() {
        assertEquals(1, Solution.jumpingOnClouds(new int[] {0, 0}));
        assertEquals(1, Solution.jumpingOnClouds(new int[] {0, 0, 0}));
        assertEquals(2, Solution.jumpingOnClouds(new int[] {0, 0, 0, 0}));
        assertEquals(2, Solution.jumpingOnClouds(new int[] {0, 0, 0, 0, 0}));
    }

    @Test
    public void jumpingOnClouds2() {
        assertEquals(3, Solution.jumpingOnClouds(new int[] {0, 0, 0, 0, 1, 0}));
        assertEquals(4, Solution.jumpingOnClouds(new int[] {0, 0, 1, 0, 0, 1, 0}));
    }
}