package repeating_string;

import org.junit.Test;

import static org.junit.Assert.*;

public class SolutionTest {

    @Test
    public void repeatedString() {
        assertEquals(1, Solution.repeatedString("a", 1));
        assertEquals(1, Solution.repeatedString("an", 2));
        assertEquals(2, Solution.repeatedString("an", 4));
        assertEquals(7, Solution.repeatedString("aba", 10));
        assertEquals(1, Solution.repeatedString("aaaa", 1));
    }
}