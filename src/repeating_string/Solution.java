package repeating_string;

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;
import java.util.stream.Stream;

public class Solution {

    // Complete the repeatedString function below.
    static long repeatedString(String s, long n) {
        int count = 0;
        for(char c: s.toCharArray()){
            if(c == 'a'){
                count++;
            }
        }

        long multiplier = n / s.length();

        long reminder = n - (multiplier * s.length());
        long amountOfAs = count * multiplier;

        String end = s.substring(0, (int) reminder);

        for(char c: end.toCharArray()){
            if(c == 'a'){
                amountOfAs++;
            }
        }

        return amountOfAs;
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        String s = scanner.nextLine();

        long n = scanner.nextLong();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        long result = repeatedString(s, n);

        bufferedWriter.write(String.valueOf(result));
        bufferedWriter.newLine();

        bufferedWriter.close();

        scanner.close();
    }
}

