package strings.funny_string;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class SolutionTest {
    @Test
    public void isFunny(){
        assertTrue(Solution.isFunny("acxz"));
        assertFalse(Solution.isFunny("bcxz"));
    }
}