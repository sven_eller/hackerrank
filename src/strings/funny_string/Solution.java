package strings.funny_string;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static java.lang.Math.abs;

public class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int count = Integer.parseInt(br.readLine());

        for (int i = 0; i < count; i++) {
            String line = br.readLine();
            if (isFunny(line)) {
                System.out.println("Funny");
            } else {
                System.out.println("Not Funny");
            }
        }
    }

    public static boolean isFunny(String input) {
        String reversed = new StringBuilder(input).reverse().toString();
        for (int i = 1; i < input.length() - 1; i++) {
            if (abs(reversed.charAt(i) - reversed.charAt(i - 1)) != abs(input.charAt(i) - input.charAt(i - 1)))
                return false;
        }
        return true;
    }
}
