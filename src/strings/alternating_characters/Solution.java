package strings.alternating_characters;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Solution {

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int count = Integer.parseInt(br.readLine());

        for (int i = 0; i < count; i++) {
            String line = br.readLine();
            System.out.println(getAlternatingCount(line));
        }
    }

    private static int getAlternatingCount(String line) {
        int toRemove = 0;
        for(int i = 1; i < line.length(); i++){
            if(line.charAt(i - 1) == line.charAt(i)){
                toRemove ++;
            }
        }
        return toRemove;
    }
}
