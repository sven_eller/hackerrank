package bit_manipulation.lonely_integer;

import java.util.*;

public class Solution {


  private static int lonelyInteger(int[] a) {
    Set<Integer> numbers = new HashSet<>();
    for (int number : a) {
      boolean contains = numbers.contains(number);
      if (contains) {
        numbers.remove(number);
      } else {
        numbers.add(number);
      }
    }
    return numbers.iterator().next();
  }

  public static void main(String[] args) {
    Scanner in = new Scanner(System.in);
    int n = in.nextInt();
    int[] a = new int[n];
    for (int i = 0; i < n; i++) {
      a[i] = in.nextInt();
    }
    System.out.println(lonelyInteger(a));
  }

}

